=======
History
=======
0.1.6 (2020-05-18)
------------------

* Add error to histograms

0.1.5 (2018-04-29)
------------------

* Fix typo

0.1.4 (2018-04-29)
------------------

* Added error message to explain crash

0.1.3 (2018-04-05)
------------------

* Easier handling of dataframe files

0.1.2 (2018-04-04)
------------------

* Updated executable name and documentation

0.1.1 (2018-10-01)
------------------

* added initial documentation

0.1.0 (2018-08-21)
------------------

* First release on PyPI.
